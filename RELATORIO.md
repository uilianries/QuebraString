# Coursera 

## ITA - TDD

### Exercício - Extração de Strings

* Inicie realizando baby-steps, 1º teste e após a função
* Minha estratégia inicial estava em validar um token numa string, e se verdadeito, extraí-lo
* Preferi utilizar Expressão Regular do que vascunhar caracter-a-caracter na string, por ser mais idiomático
* Precisei refatorar a validação de Pattern, pois estava sendo replicada em 3 pontos do código.
* Ao precisar tratar a string "numeroCPFComposto" noite que seria necessário aumentar a complexidade da função,
  pois haveriam 3 tokens na mesma palavra. Isso resultou numa adoção de nova estratégia.
* A nova estratégia está em extrair tokens diretamente da string e armazenar na lista.
