package org.ita.quebra;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Realizza quebra de string em formato CamelCase
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
public class QuebraString {

    /**
     * Quebra string e retorna lista de tokens
     * @param string String em formato CamelCase
     * @return Lista de tokens
     */
    public static List<String> quebrar(String string) {
        List<String> tokens = new ArrayList<String>();
        if (!isValida(string)) {
            throw new InvalidParameterException("String inválida");
        }
        extrairCaixaBaixa(tokens, string);
        extrairCaixaAlta(tokens, string);
        extrairNumero(tokens, string);
        extrairCamelCase(tokens, string);
        return tokens;
    }

    /**
     * Extrair pattern de uma palavra
     * @param tokens Onde serão armazenadas as palavras extraidas
     * @param string Palavra que deve ser verificada
     * @param pattern Pattern para ser encontrado
     * @return Palavra com token removido
     */
    private static void extrairPattern(List<String> tokens, String string, String pattern) {
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(string);
        while (matcher.find()) {
            tokens.add(matcher.group());
        }
    }

    /**
     * Extrair palavras Camel Case
     * @param tokens Onde serão armazenadas as palavras extraidas
     * @param string Palavra que deve ser verificada
     * @return Palavra com token removido
     */
    private static void extrairCamelCase(List<String> tokens, String string) {
        String pattern = "[A-Z][a-z]+";
        List<String> words = new ArrayList<String>();
        extrairPattern(words, string, pattern);
        for (String word : words) {
            tokens.add(word.toLowerCase());
        }
    }

    /**
     * Extrair palavras Caixa Alta
     * @param tokens Onde serão armazenadas as palavras extraidas
     * @param string Palavra que deve ser verificada
     * @return Palavra com token removido
     */
    private static void extrairCaixaAlta(List<String> tokens, String string) {
        String pattern = "[A-Z]{3}";
        extrairPattern(tokens, string, pattern);
    }

    /**
     * Extrair palavras Caixa Baixa
     * @param tokens Onde serão armazenadas as palavras extraidas
     * @param string Palavra que deve ser verificada
     * @return Palavra com token removido
     */
    private static void extrairCaixaBaixa(List<String> tokens, String string) {
        String pattern = "\\b[a-z]+";
        extrairPattern(tokens, string, pattern);
    }

    /**
     * Extrair palavras Caixa Baixa
     * @param tokens Onde serão armazenadas as palavras extraidas
     * @param string Palavra que deve ser verificada
     * @return Palavra com token removido
     */
    private static void extrairNumero(List<String> tokens, String string) {
        String pattern = "\\d+";
        extrairPattern(tokens, string, pattern);
    }

    /**
     * Valida construção da string
     * @param string Conjunto para validação
     * @return true se não iniciar com numeros
     */
    private static boolean isValida(String string) {
        Pattern regex = Pattern.compile("(^\\d)|(\\W)");
        Matcher matcher = regex.matcher(string);
        return !matcher.find();
    }
}
