package org.ita.quebra;

import org.junit.Test;

import java.security.InvalidParameterException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Realiza validação da quebra de string
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
public class TestQuebraString {

    @Test
    public void quebraNomeCaixaBaixa() {
        final String nome = "nome";
        List<String> lista = QuebraString.quebrar(nome);
        assertEquals(1, lista.size());
        assertTrue(nome.equals(lista.get(0)));
    }

    @Test
    public void quebraNomeCamelCase() {
        final String nome = "Nome";
        List<String> lista = QuebraString.quebrar(nome);
        assertEquals(1, lista.size());
        assertTrue(nome.toLowerCase().equals(lista.get(0)));
    }

    @Test
    public void quebraNomeComposto() {
        final String nome = "nome";
        final String composto = "Composto";
        List<String> lista = QuebraString.quebrar(nome + composto);
        assertEquals(2, lista.size());
        assertTrue(nome.equals(lista.get(0)));
        assertTrue(composto.toLowerCase().equals(lista.get(1)));
    }

    @Test
    public void quebraNomeCompostoCaixaAlta() {
        final String nome = "Nome";
        final String composto = "Composto";
        List<String> lista = QuebraString.quebrar(nome + composto);
        assertEquals(2, lista.size());
        assertTrue(nome.toLowerCase().equals(lista.get(0)));
        assertTrue(composto.toLowerCase().equals(lista.get(1)));
    }

    @Test
    public void quebraCPF() {
        final String nome = "CPF";
        List<String> lista = QuebraString.quebrar(nome);
        assertEquals(1, lista.size());
        assertTrue(nome.equals(lista.get(0)));
    }

    @Test
    public void quebraCPFCompostoContribuinte() {
        final String numero = "numero";
        final String cpf = "CPF";
        final String contribuinte = "Contribuinte";
        List<String> lista = QuebraString.quebrar(numero + cpf + contribuinte);
        assertEquals(3, lista.size());
        assertTrue(numero.equals(lista.get(0)));
        assertTrue(cpf.equals(lista.get(1)));
        assertTrue(contribuinte.toLowerCase().equals(lista.get(2)));
    }

    @Test
    public void quebraNumero() {
        final String recupera = "recupera";
        final String dez = "10";
        final String primeiros = "Primeiros";
        List<String> lista = QuebraString.quebrar(recupera + dez + primeiros);
        assertEquals(3, lista.size());
        assertTrue(recupera.equals(lista.get(0)));
        assertTrue(dez.equals(lista.get(1)));
        assertTrue(primeiros.toLowerCase().equals(lista.get(2)));
    }

    @Test(expected=InvalidParameterException.class)
    public void validaNumero() {
        final String dez = "10";
        final String primeiros = "Primeiros";
        QuebraString.quebrar(dez + primeiros);
    }

    @Test(expected=InvalidParameterException.class)
    public void validaCaracter() {
        final String nome = "nome";
        final String hashtag = "#";
        final String composto = "Composto";
        QuebraString.quebrar(nome + hashtag + composto);
    }
}